﻿using System.ComponentModel.DataAnnotations;
using TakeaNoteWS.Utils.ResourceFiles;

namespace TakeaNoteWS.Services.Api.Models
{
    public class UserLoginModel
    {
        [Required(ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "LoginFieldRequired")]
        public string Login { get; set; }

        [Required(ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "PasswordFieldRequired")]
        public string Password { get; set; }      
    }
}
