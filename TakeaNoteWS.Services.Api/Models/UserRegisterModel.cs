﻿using System.ComponentModel.DataAnnotations;
using TakeaNoteWS.Utils.ResourceFiles;

namespace TakeaNoteWS.Services.Api.Models
{
    public class UserRegisterModel
    {
        [Required(ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "NameFieldRequired")]

        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "LoginFieldRequired")]
        public string Login { get; set; }

        [Required(ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "EmailFieldRequired")]
        [EmailAddress(ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "EmailFieldNotValid")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "PasswordFieldRequired")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "PasswordAndConfirmationPasswordFieldsDoNotMatch")]
        public string PasswordConfirm { get; set; }
    }
}
