﻿using System;

namespace TakeaNoteWS.Services.Api.Models
{
    public class UserDataModel
    {
        public int IdUser { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string Role { get; set; }

        public int TotalNotes { get; set; }
        public int TotalCategories { get; set; }
    }
}
