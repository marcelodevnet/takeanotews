﻿using System.ComponentModel.DataAnnotations;
using TakeaNoteWS.Utils.ResourceFiles;

namespace TakeaNoteWS.Services.Api.Models
{
    public class UserUpdateModel
    {
        [Required(ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "IdUserFieldRequired")]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "InvalidInputNumber")]
        public string IdUser { get; set; }

        [Required(ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "NameFieldRequired")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "LoginFieldRequired")]
        public string Login { get; set; }

        [Required(ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "EmailFieldRequired")]
        [EmailAddress(ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "EmailFieldNotValid")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "CurrentPasswordFieldRequired")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }

        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(UserResource), ErrorMessageResourceName = "RoleFieldRequired")]
        [StringLength(1, ErrorMessage = "The {0} must be exactly {2} characters long.", MinimumLength = 1)]
        public string Role { get; set; }

    }
}
