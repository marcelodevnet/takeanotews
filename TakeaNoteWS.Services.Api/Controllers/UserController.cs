﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TakeaNoteWS.Application.Contracts;
using TakeaNoteWS.Domain.Entities;
using TakeaNoteWS.Services.Api.Models;
using TakeaNoteWS.Utils.ResourceFiles;

namespace TakeaNoteWS.Services.Api.Controllers
{
    //[Authorize("Bearer", Roles = "U")]
    [Authorize("Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserAppService userAppService;
        private readonly INoteAppService noteAppService;
        private readonly ICategoryAppService categoryAppService;
        private readonly IMapper mapper;

        public UserController(IUserAppService userAppService, INoteAppService noteAppService, ICategoryAppService categoryAppService, IMapper mapper)
        {
            this.userAppService = userAppService;
            this.noteAppService = noteAppService;
            this.categoryAppService = categoryAppService;
            this.mapper = mapper;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Get(int id)
        {
            try
            {
                var model = mapper.Map<UserDataModel>(userAppService.FindById(id));

                if(model != null)
                {
                    model.TotalNotes = noteAppService.Count(x => x.IdUser == id);
                    model.TotalCategories = categoryAppService.Count(x => x.IdUser == id);

                    return Ok(model);
                }

                return BadRequest(UserResource.UserNotFound);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Get()
        {
            try
            {
                var model = mapper.Map<ICollection<UserDataModel>>(userAppService.FindAll());

                foreach (var item in model)
                {
                    item.TotalNotes = noteAppService.Count(x => x.IdUser == item.IdUser);
                    item.TotalCategories = categoryAppService.Count(x => x.IdUser == item.IdUser);
                }

                return Ok(model);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Post([FromBody] UserRegisterModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = mapper.Map<User>(model);

                    userAppService.Insert(user);

                    return Ok(UserResource.UserRegisteredSuccessfully);
                }
                catch (Exception ex)
                {
                    return StatusCode(500, ex.Message);
                }
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Put([FromBody] UserUpdateModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = mapper.Map<User>(model);

                    userAppService.Update(user);

                    return Ok(UserResource.UserUpdatedSuccessfully);
                }
                catch (Exception ex)
                {
                    return StatusCode(500, ex.Message);
                }
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Delete(int id)
        {
            try
            {
                //TODO: Overwrite Delete methods to use ID instead obj

                var user = userAppService.FindById(id);

                if (user != null)
                {
                    userAppService.Delete(user);

                    return Ok(UserResource.UserDeletedSuccessfully);
                }
                else
                {
                    return BadRequest(UserResource.UserNotFound);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}