﻿using AutoMapper;
using TakeaNoteWS.Domain.Entities;
using TakeaNoteWS.Services.Api.Models;

namespace TakeaNoteWS.Services.Api.Mappings
{
    public class EntityToModelProfile : Profile
    {
        public EntityToModelProfile()
        {
            CreateMap<User, UserDataModel>();
        }
    }
}
