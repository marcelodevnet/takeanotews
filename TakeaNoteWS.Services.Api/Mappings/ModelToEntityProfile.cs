﻿using AutoMapper;
using TakeaNoteWS.Domain.Entities;
using TakeaNoteWS.Services.Api.Models;

namespace TakeaNoteWS.Services.Api.Mappings
{
    public class ModelToEntityProfile : Profile
    {
        public ModelToEntityProfile()
        {
            CreateMap<UserRegisterModel, User>();
            CreateMap<UserUpdateModel, User>()
                .AfterMap((src, dest) => dest.Password = src.CurrentPassword);
        }
    }
}
