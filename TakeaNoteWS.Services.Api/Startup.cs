﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using TakeaNoteWS.Application.Contracts;
using TakeaNoteWS.Application.Services;
using TakeaNoteWS.Domain.Contracts;
using TakeaNoteWS.Domain.Contracts.Repositories;
using TakeaNoteWS.Domain.Contracts.Services;
using TakeaNoteWS.Domain.Services;
using TakeaNoteWS.Infra.Data.Dapper.Repositories;
using TakeaNoteWS.Infra.Data.EntityFramework.Context;
using TakeaNoteWS.Infra.Data.EntityFramework.Repositories;
using TakeaNoteWS.Infra.Identity;
using TakeaNoteWS.Utils;

namespace TakeaNoteWS.Services.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            #region Repository Dependencies Map

            services.AddTransient<INoteRepository, NoteRepository>();
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IUserRepository, UserRepository>();

            services.AddTransient<INoteRepositoryDapper, NoteRepositoryDapper>();
            services.AddTransient<ICategoryRepositoryDapper, CategoryRepositoryDapper>();
            services.AddTransient<IUserRepositoryDapper, UserRepositoryDapper>();

            services.AddTransient<IUnitOfWorkDapper, UnitOfWorkDapper>();
            services.AddTransient<IUnitOfWorkEF, UnitOfWorkEF>();

            services.AddTransient<DataContext>();
            services.AddTransient<ICryptorEngine, CryptorEngine>();

            services.AddTransient<INoteDomainService, NoteDomainService>();
            services.AddTransient<ICategoryDomainService, CategoryDomainService>();
            services.AddTransient<IUserDomainService, UserDomainService>();

            services.AddTransient<INoteAppService, NoteAppService>();
            services.AddTransient<ICategoryAppService, CategoryAppService>();
            services.AddTransient<IUserAppService, UserAppService>();

            #endregion

            services.AddDbContext<DataContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("takeanote"))
                       .UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll));

            services.AddTransient(db => new SqlConnection(Configuration.GetConnectionString("takeanote")));
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            #region AppSettings

            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            #endregion

            #region JWT

            var signingConfigurations = new LoginConfiguration();

            services.AddSingleton(signingConfigurations);

            var tokenConfigurations = new TokenConfiguration();

            new ConfigureFromConfigurationOptions<TokenConfiguration>(
                Configuration.GetSection("TokenConfiguration"))
                .Configure(tokenConfigurations);

            services.AddSingleton(tokenConfigurations);

            services.AddAuthentication(authOptions =>
            {
                authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(bearerOptions =>
                {
                    var paramsValidation = bearerOptions.TokenValidationParameters;

                    paramsValidation.IssuerSigningKey = signingConfigurations.Key;
                    paramsValidation.ValidAudience = tokenConfigurations.Audience;
                    paramsValidation.ValidIssuer = tokenConfigurations.Issuer;

                    // Valida a assinatura de um token recebido
                    paramsValidation.ValidateIssuerSigningKey = true;

                    // Verifica se um token recebido ainda é válido
                    paramsValidation.ValidateLifetime = true;

                    // Tempo de tolerância para a expiração de um token (utilizado 
                    // caso haja problemas de sincronismo de horário entre diferentes
                    // computadores envolvidos no processo de comunicação)
                    paramsValidation.ClockSkew = TimeSpan.Zero;
                });

            // Ativa o uso do token como forma de autorizar o acesso
            // a recursos deste projeto
            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .RequireRole(new string[] { "C", "U", "A", "M" })
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .Build());
            });

            #endregion

            #region Swagger

            services.AddSwaggerGen(
                s =>
                {
                    s.SwaggerDoc("v1",
                    new Info
                    {
                        Title = "Take a Note WS",
                        Description = "Take a Note WS",
                        Version = "v2",
                        Contact = new Contact
                        {
                            Name = "Marcelo Oliveira",
                            Email = "marcelouff@outlook.com",
                            Url = "https://www.linkedin.com/in/marcelo-oliveira-de-araujo-133665130/"
                        }
                    });

                    //Locate the XML file being generated by ASP.NET...
                    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                    //... and tell Swagger to use those XML comments.
                    s.IncludeXmlComments(xmlPath);

                    s.OperationFilter<AddHeaderParameter>();
                });

            #endregion

            #region AutoMapper

            services.AddAutoMapper(typeof(Startup));

            #endregion

            // ASP.NET HttpContext dependency
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUser, AspNetUser>();


        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthentication();

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(
            s =>
            {
                s.SwaggerEndpoint("/swagger/v1/swagger.json", "projeto");
            });
        }
    }
}
