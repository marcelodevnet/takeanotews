﻿using System.Data.SqlClient;
using TakeaNoteWS.Domain.Contracts.Repositories;

namespace TakeaNoteWS.Infra.Data.Dapper.Repositories
{
    public class CategoryRepositoryDapper : ICategoryRepositoryDapper
    {
        private readonly SqlConnection connection;
        private readonly SqlTransaction transaction;

        public CategoryRepositoryDapper(SqlConnection connection, SqlTransaction transaction)
        {
            this.connection = connection;
            this.transaction = transaction;
        }

        public void Dispose()
        {
            connection.Dispose();
        }
    }
}
