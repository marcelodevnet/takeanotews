﻿using System.Data.SqlClient;
using TakeaNoteWS.Domain.Contracts.Repositories;

namespace TakeaNoteWS.Infra.Data.Dapper.Repositories
{
    public class UnitOfWorkDapper : IUnitOfWorkDapper
    {
        private readonly SqlConnection connection;
        private SqlTransaction transaction;

        public UnitOfWorkDapper(SqlConnection connection)
        {
            this.connection = connection;
        }

        public IUserRepositoryDapper UserRepositoryDapper => new UserRepositoryDapper(connection, transaction);
        public INoteRepositoryDapper NoteRepositoryDapper => new NoteRepositoryDapper(connection, transaction);
        public ICategoryRepositoryDapper CategoryRepositoryDapper => new CategoryRepositoryDapper(connection, transaction);

        public void BeginTransaction()
        {
            transaction = connection.BeginTransaction();
        }
        public void Commit()
        {
            transaction.Commit();
        }
        public void Rollback()
        {
            transaction.Rollback();
        }

        public void Dispose()
        {
            connection.Dispose();
        }
    }
}
