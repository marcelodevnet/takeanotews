﻿using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Application.Contracts
{
    public interface ICategoryAppService : IBaseAppService<Category, int>
    {
    }
}
