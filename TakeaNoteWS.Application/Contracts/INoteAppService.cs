﻿using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Application.Contracts
{
    public interface INoteAppService : IBaseAppService<Note, int>
    {
    }
}
