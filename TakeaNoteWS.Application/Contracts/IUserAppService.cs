﻿using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Application.Contracts
{
    public interface IUserAppService : IBaseAppService<User, int>
    {
        User FindByLoginAndPassword(string login, string password);
    }
}
