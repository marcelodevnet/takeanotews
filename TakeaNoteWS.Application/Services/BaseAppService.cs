﻿using System;
using System.Collections.Generic;
using TakeaNoteWS.Application.Contracts;
using TakeaNoteWS.Domain.Contracts.Services;

namespace TakeaNoteWS.Application.Services
{
    public abstract class BaseAppService<TEntity, TKey> : IBaseAppService<TEntity, TKey> where TEntity : class
    {
        private readonly IBaseDomainService<TEntity, TKey> domain;

        protected BaseAppService(IBaseDomainService<TEntity, TKey> domain)
        {
            this.domain = domain;
        }

        public virtual void Insert(TEntity obj)
        {
            domain.Insert(obj);
        }
        public virtual void Update(TEntity obj)
        {
            domain.Update(obj);
        }
        public virtual void Delete(TEntity obj)
        {
            domain.Delete(obj);
        }
        public virtual TEntity FindById(TKey id)
        {
            return domain.FindById(id);
        }
        public virtual ICollection<TEntity> FindAll()
        {
            return domain.FindAll();
        }
        public virtual int Count(Func<TEntity, bool> predicate)
        {
            return domain.Count(predicate);
        }
        public virtual void Dispose()
        {
            domain.Dispose();
        }
    }
}
