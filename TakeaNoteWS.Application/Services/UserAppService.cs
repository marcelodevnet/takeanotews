﻿using TakeaNoteWS.Application.Contracts;
using TakeaNoteWS.Domain.Contracts.Services;
using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Application.Services
{
    public class UserAppService : BaseAppService<User, int>, IUserAppService
    {
        private readonly IUserDomainService domain;

        public UserAppService(IUserDomainService domain) : base(domain)
        {
            this.domain = domain;
        }

        public User FindByLoginAndPassword(string login, string password)
        {
            return domain.FindByLoginAndPassword(login, password);
        }
    }
}
