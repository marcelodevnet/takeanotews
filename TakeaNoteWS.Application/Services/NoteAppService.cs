﻿using TakeaNoteWS.Application.Contracts;
using TakeaNoteWS.Domain.Contracts.Services;
using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Application.Services
{
    public class NoteAppService : BaseAppService<Note, int>, INoteAppService
    {
        private readonly INoteDomainService domain;

        public NoteAppService(INoteDomainService domain) : base(domain)
        {
            this.domain = domain;
        }  
    }
}
