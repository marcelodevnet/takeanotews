﻿using TakeaNoteWS.Application.Contracts;
using TakeaNoteWS.Domain.Contracts.Services;
using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Application.Services
{
    public class CategoryAppService : BaseAppService<Category, int>, ICategoryAppService
    {
        private readonly ICategoryDomainService domain;

        public CategoryAppService(ICategoryDomainService domain) : base(domain)
        {
            this.domain = domain;
        }
    }
}
