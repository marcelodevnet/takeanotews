﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace TakeaNoteWS.Infra.Data.EntityFramework.Context
{
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        //private readonly string sqlServerConnection;
        //public DataContextFactory(IConfiguration configuration)
        //{
        //    sqlServerConnection = configuration.GetConnectionString("takeanote");
        //}
        //public DataContext CreateDbContext(string[] args)
        //{
        //    var builder = new DbContextOptionsBuilder<DataContext>();
        //    builder.UseSqlServer(sqlServerConnection);
        //    return new DataContext(builder.Options);
        //}


        //TODO: Consider remove this, Workaround solution for Add-Migration and Update-databse EF-Core 2.2
        public DataContext CreateDbContext(params string[] args)
        {
            var options = new DbContextOptionsBuilder<DataContext>();
            var config = GetAppConfiguration();

            options.UseSqlServer(config.GetConnectionString("takeanote"));

            return new DataContext(options.Options);
        }

        IConfiguration GetAppConfiguration()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var dir = Directory.GetParent(AppContext.BaseDirectory);

            do
                dir = dir.Parent;
            while (dir.Name != "bin");

            dir = dir.Parent;

            var path = dir.FullName;

            var builder = new ConfigurationBuilder()
                .SetBasePath(path)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{environmentName}.json", true);

            return builder.Build();
        }
    }
}
