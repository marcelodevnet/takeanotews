﻿using Microsoft.EntityFrameworkCore;
using TakeaNoteWS.Domain.Entities;
using TakeaNoteWS.Infra.Data.EntityFramework.Configurations;

namespace TakeaNoteWS.Infra.Data.EntityFramework.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> builder) : base(builder)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new NoteConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryConfiguration());
            modelBuilder.ApplyConfiguration(new NoteCategoryConfiguration());
        }

        public DbSet<User> User { get; set; }
        public DbSet<Note> Note { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<NoteCategory> NoteCategory { get; set; }
    }
}
