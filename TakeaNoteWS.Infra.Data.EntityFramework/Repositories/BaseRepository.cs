﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TakeaNoteWS.Domain.Contracts.Repositories;
using TakeaNoteWS.Infra.Data.EntityFramework.Context;

namespace TakeaNoteWS.Infra.Data.EntityFramework.Repositories
{
    public abstract class BaseRepository<TEntity, TKey> : IBaseRepository<TEntity, TKey> where TEntity : class
    {
        private readonly DataContext context;

        public BaseRepository(DataContext context)
        {
            this.context = context;
        }

        public void Insert(TEntity obj)
        {
            context.Entry(obj).State = EntityState.Added;
            context.SaveChanges();
        }

        public void InsertRange(ICollection<TEntity> collection)
        {
            context.Set<TEntity>().AddRange(collection);
            context.SaveChanges();
        }

        public void Update(TEntity obj)
        {
            context.Entry(obj).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Update(TEntity obj, params Expression<Func<TEntity, object>>[] expressions)
        {
            var item = context.Attach(obj);

            foreach (var expression in expressions)
            {
                item.Property(expression).IsModified = true;
            }

            //TODO: Verificar se funciona
            item.Context.SaveChanges();
        }

        public void Delete(TEntity obj)
        {
            context.Entry(obj).State = EntityState.Deleted;
            context.SaveChanges();
        }

        public void Delete(Func<TEntity, bool> predicate)
        {
            context.Set<TEntity>()
           .Where(predicate).ToList()
           .ForEach(x => context.Set<TEntity>().Remove(x));

            context.SaveChanges();
        }

        public void DeleteRange(ICollection<TEntity> collection)
        {
            context.Set<TEntity>().RemoveRange(collection);
            context.SaveChanges();
        }

        public TEntity Find(Func<TEntity, bool> predicate)
        {
            return context.Set<TEntity>().FirstOrDefault(predicate);
        }

        public TEntity FindById(TKey id)
        {
            return context.Set<TEntity>().Find(id);
        }

        public ICollection<TEntity> FindAll()
        {
            return context.Set<TEntity>().ToList();
        }

        public ICollection<TEntity> FindAll(Func<TEntity, bool> predicate)
        {
            return context.Set<TEntity>().Where(predicate).ToList();
        }


        public int Count()
        {
            return context.Set<TEntity>().Count();
        }

        public int Count(Func<TEntity, bool> predicate)
        {
            return context.Set<TEntity>().Count(predicate);
        }

        public void Dispose()
        {
            context.Dispose();
        }

    }
}
