﻿using TakeaNoteWS.Domain.Contracts.Repositories;
using TakeaNoteWS.Domain.Entities;
using TakeaNoteWS.Infra.Data.EntityFramework.Context;

namespace TakeaNoteWS.Infra.Data.EntityFramework.Repositories
{
    public class CategoryRepository : BaseRepository<Category, int>, ICategoryRepository
    {
        private readonly DataContext context;
        public CategoryRepository(DataContext context) : base(context)
        {
            this.context = context;
        }
    }
}
