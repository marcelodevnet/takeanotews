﻿using Microsoft.EntityFrameworkCore.Storage;
using TakeaNoteWS.Domain.Contracts.Repositories;
using TakeaNoteWS.Infra.Data.EntityFramework.Context;

namespace TakeaNoteWS.Infra.Data.EntityFramework.Repositories
{
    public class UnitOfWorkEF : IUnitOfWorkEF
    {
        private readonly DataContext context;
        private IDbContextTransaction transaction;

        public UnitOfWorkEF(DataContext context)
        {
            this.context = context;
        }

        public IUserRepository UserRepository => new UserRepository(context);
        public INoteRepository NoteRepository => new NoteRepository(context);
        public ICategoryRepository CategoryRepository => new CategoryRepository(context);


        public void BeginTransaction()
        {
            transaction = context.Database.BeginTransaction();
        }
        public void Commit()
        {
            transaction.Commit();
        }
        public void Rollback()
        {
            transaction.Rollback();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
