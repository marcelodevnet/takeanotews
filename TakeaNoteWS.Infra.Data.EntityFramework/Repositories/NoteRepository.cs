﻿using TakeaNoteWS.Domain.Contracts.Repositories;
using TakeaNoteWS.Domain.Entities;
using TakeaNoteWS.Infra.Data.EntityFramework.Context;

namespace TakeaNoteWS.Infra.Data.EntityFramework.Repositories
{
    public class NoteRepository : BaseRepository<Note, int>, INoteRepository
    {
        private readonly DataContext context;
        public NoteRepository(DataContext context) : base(context)
        {
            this.context = context;
        }
    }
}
