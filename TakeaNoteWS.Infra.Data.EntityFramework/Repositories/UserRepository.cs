﻿using TakeaNoteWS.Domain.Contracts.Repositories;
using TakeaNoteWS.Domain.Entities;
using TakeaNoteWS.Infra.Data.EntityFramework.Context;

namespace TakeaNoteWS.Infra.Data.EntityFramework.Repositories
{
    public class UserRepository : BaseRepository<User, int>, IUserRepository
    {
        private readonly DataContext context;
        public UserRepository(DataContext context) : base(context)
        {
            this.context = context;
        }
    }
}
