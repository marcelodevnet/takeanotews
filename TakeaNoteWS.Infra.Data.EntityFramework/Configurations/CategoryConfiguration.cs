﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Infra.Data.EntityFramework.Configurations
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {

            builder.HasKey(c => c.IdCategory);

            builder.Property(c => c.IdCategory)
                .HasColumnName("IdCategory");

            builder.Property(c => c.Name)
                .HasColumnName("Name")
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(c => c.Description)
                .HasColumnName("Description")
                .HasMaxLength(250)
                .IsRequired();

            builder.Property(c => c.RegistrationDate)
                .HasColumnName("RegistrationDate")
                .HasColumnType("datetime")
                .IsRequired();

            builder.Property(c => c.Visualization)
               .HasColumnName("Visualization")
               .HasMaxLength(1)
               .IsFixedLength()
               .IsRequired();

            builder.Property(c => c.IdUser)
                .HasColumnName("IdUser")
                .IsRequired();

            // One to Many relationship..

            builder.HasOne(u => u.User)
                .WithMany(c => c.CategoryCollection)
                .HasForeignKey(u => u.IdUser)
                .OnDelete(DeleteBehavior.Restrict);


        }
    }
}
