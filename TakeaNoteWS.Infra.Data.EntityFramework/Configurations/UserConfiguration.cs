﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Infra.Data.EntityFramework.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => new { u.IdUser });

            builder.Property(u => u.IdUser)
                .HasColumnName("IdUser");

            builder.Property(u => u.Name)
                .HasColumnName("Name")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(u => u.Email)
                .HasColumnName("Email")
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(u => u.Login)
               .HasColumnName("Login")
               .HasMaxLength(50)
               .IsRequired();

            builder.Property(u => u.Password)
               .HasColumnName("Password")
               .HasMaxLength(50)
               .IsRequired();

            builder.Property(u => u.RegistrationDate)
               .HasColumnName("RegistrationDate")
               .HasColumnType("datetime")
               .IsRequired();

            builder.Property(u => u.Role)
               .HasColumnName("Role")
               .HasMaxLength(1)
               .IsFixedLength()
               .IsRequired();
        }
    }
}
