﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Infra.Data.EntityFramework.Configurations
{
    public class NoteCategoryConfiguration : IEntityTypeConfiguration<NoteCategory>
    {
        public void Configure(EntityTypeBuilder<NoteCategory> builder)
        {
            builder.HasKey(nc => new { nc.IdNote, nc.IdCategory });

            // Many to Many relationship..

            builder.HasOne(nc => nc.Note)
                .WithMany(s => s.NoteCategoryCollection)
                .HasForeignKey(nc => nc.IdNote)
                .OnDelete(DeleteBehavior.Restrict);


            // Many to Many relationship..

            builder.HasOne(nc => nc.Category)
                .WithMany(s => s.NoteCategoryCollection)
                .HasForeignKey(nc => nc.IdCategory)
                .OnDelete(DeleteBehavior.Restrict);

        }
    }
}
