﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Infra.Data.EntityFramework.Configurations
{
    public class NoteConfiguration : IEntityTypeConfiguration<Note>
    {
        public void Configure(EntityTypeBuilder<Note> builder)
        {
            builder.HasKey(n => n.IdNote);

            builder.Property(n => n.IdNote)
                .HasColumnName("IdNote");

            builder.Property(n => n.Description)
                .HasColumnName("Description")
                .HasMaxLength(250).IsRequired();

            builder.Property(n => n.Author)
                .HasColumnName("Author")
                .HasMaxLength(100);

            builder.Property(n => n.RegistrationDate)
                .HasColumnName("RegistrationDate")
                .HasColumnType("datetime")
                .IsRequired();

            builder.Property(n => n.Visualization)
               .HasColumnName("Visualization")
               .HasMaxLength(1)
               .IsFixedLength()
               .IsRequired();

            builder.Property(n => n.IdUser)
                .HasColumnName("IdUser")
                .IsRequired();

            // One to Many relationship..

            builder.HasOne(u => u.User)
                .WithMany(n => n.NoteCollection)
                .HasForeignKey(u => u.IdUser)
                .OnDelete(DeleteBehavior.Restrict);



        }
    }
}
