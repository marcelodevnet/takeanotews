﻿using System;
using System.Collections.Generic;

namespace TakeaNoteWS.Domain.Entities
{
    public class Note
    {
        public virtual int IdNote { get; set; }
        public virtual string Description { get; set; }
        public virtual string Author { get; set; }
        public virtual DateTime RegistrationDate { get; set; }
        public virtual string Visualization { get; set; }
        public virtual int IdUser { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<NoteCategory> NoteCategoryCollection { get; set; }
    }
}
