﻿using System;
using System.Collections.Generic;

namespace TakeaNoteWS.Domain.Entities
{
    public class User
    {
        public virtual int IdUser { get; set; }
        public virtual string Name { get; set; }
        public virtual string Login { get; set; }
        public virtual string Email { get; set; }
        public virtual string Password { get; set; }
        public virtual DateTime RegistrationDate { get; set; }
        public virtual string Role { get; set; }

        public virtual ICollection<Note> NoteCollection { get; set; }
        public virtual ICollection<Category> CategoryCollection { get; set; }

    }
}
