﻿namespace TakeaNoteWS.Domain.Entities
{
    public class NoteCategory
    {
        public virtual int IdNote { get; set; }
        public virtual Note Note { get; set; }

        public virtual int IdCategory { get; set; }
        public virtual Category Category { get; set; }
    }
}
