﻿using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Security.Claims;
using TakeaNoteWS.Domain.Contracts;
using TakeaNoteWS.Domain.Contracts.Repositories;
using TakeaNoteWS.Domain.Contracts.Services;
using TakeaNoteWS.Domain.Entities;
using TakeaNoteWS.Utils;
using TakeaNoteWS.Utils.ResourceFiles;

namespace TakeaNoteWS.Domain.Services
{
    public class UserDomainService : BaseDomainService<User, int>, IUserDomainService
    {
        private readonly IUnitOfWorkEF unitOfWorkEF;
        private readonly IUnitOfWorkDapper unitOfWorkDapper;
        private readonly ICryptorEngine cryptorEngine;
        private readonly IOptions<AppSettings> appSettings;
        private readonly IUser loggedUser;


        public UserDomainService(IUnitOfWorkEF unitOfWorkEF, IUnitOfWorkDapper unitOfWorkDapper,
            ICryptorEngine cryptorEngine, IOptions<AppSettings> appSettings, IUser loggedUser)
            : base(unitOfWorkEF.UserRepository)
        {
            this.unitOfWorkEF = unitOfWorkEF;
            this.unitOfWorkDapper = unitOfWorkDapper;
            this.cryptorEngine = cryptorEngine;
            this.appSettings = appSettings;
            this.loggedUser = loggedUser;
        }

        public override void Insert(User obj)
        {
            obj.RegistrationDate = DateTime.Now;
            obj.Role = appSettings.Value.NewUsersRole;
            obj.Password = cryptorEngine.Encrypt(obj.Password, true);

            if (HasLogin(obj.Login))
            {
                throw new Exception(UserResource.LoginAlreadyInUse);
            }

            if (HasEmail(obj.Email))
            {
                throw new Exception(UserResource.EmailAlreadyInUse);
            }

            unitOfWorkEF.UserRepository.Insert(obj);
        }

        public override void Update(User obj)
        {
            var currentUser = unitOfWorkEF.UserRepository.FindById(obj.IdUser);

            //TODO: Specification: Only Master and Admin users can edit another users info
            //TODO: Specification: Only Master users can edit Login and Role field from any user
            //TODO: Prevent low level users from editing another users data

            var loggedUserRole = loggedUser.GetUserRoles().FirstOrDefault();

            if (loggedUser.Name != obj.Login)
            {
                if (loggedUserRole != "M" && loggedUserRole != "A")
                    throw new Exception(UserResource.AccessDeniedEditAnotherUsers);

            }

            if (!string.IsNullOrEmpty(obj.Login) || !string.IsNullOrEmpty(obj.Role))
            {
                if(loggedUserRole != "M")
                    throw new Exception(UserResource.AccessDeniedEditProtectedFields);
            }

            if (currentUser != null)
            {
                currentUser.Name = obj.Name;
                currentUser.Email = obj.Email;
                currentUser.Password = cryptorEngine.Encrypt(obj.Password, true);

                if (loggedUserRole == "M")
                {
                    currentUser.Login = obj.Login;
                    currentUser.Role = obj.Role;
                }

                unitOfWorkEF.UserRepository.Update(currentUser);
            }
            else
            {
                throw new Exception(UserResource.UserNotFound);
            }
        }

        public override void Delete(User obj)
        {
            unitOfWorkEF.UserRepository.Delete(obj);

        }

        //TODO: SPECIFICATION
        protected bool HasLogin(string login)
        {
            return unitOfWorkEF.UserRepository.Count(x => x.Login.Equals(login)) > 0;
        }

        //TODO: SPECIFICATION
        protected bool HasEmail(string email)
        {
            return unitOfWorkEF.UserRepository.Count(x => x.Email.Equals(email)) > 0;
        }

        public User FindByLoginAndPassword(string login, string password)
        {
            password = cryptorEngine.Encrypt(password, true);

            return unitOfWorkEF.UserRepository.Find(x => x.Login == login && x.Password == password);
        }
    }
}
