﻿using System;
using System.Collections.Generic;
using TakeaNoteWS.Domain.Contracts.Repositories;
using TakeaNoteWS.Domain.Contracts.Services;

namespace TakeaNoteWS.Domain.Services
{
    public abstract class BaseDomainService<TEntity, TKey> : IBaseDomainService<TEntity, TKey> where TEntity : class
    {
        private readonly IBaseRepository<TEntity, TKey> repository;

        protected BaseDomainService(IBaseRepository<TEntity, TKey> repository)
        {
            this.repository = repository;
        }

        public virtual void Insert(TEntity obj)
        {
            repository.Insert(obj);
        }
        public virtual void Update(TEntity obj)
        {
            repository.Update(obj);
        }
        public virtual void Delete(TEntity obj)
        {
            repository.Delete(obj);
        }
        public virtual ICollection<TEntity> FindAll()
        {
            return repository.FindAll();
        }
        public virtual TEntity FindById(TKey id)
        {
            return repository.FindById(id);
        }
        public virtual void Dispose()
        {
            repository.Dispose();
        }
        public virtual int Count(Func<TEntity, bool> predicate)
        {
            return repository.Count(predicate);
        }
    }
}
