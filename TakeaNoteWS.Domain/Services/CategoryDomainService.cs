﻿using TakeaNoteWS.Domain.Contracts.Repositories;
using TakeaNoteWS.Domain.Contracts.Services;
using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Domain.Services
{
    public class CategoryDomainService : BaseDomainService<Category, int>, ICategoryDomainService
    {
        private readonly IUnitOfWorkEF unitOfWork;

        public CategoryDomainService(IUnitOfWorkEF unitOfWork) : base(unitOfWork.CategoryRepository)
        {
            this.unitOfWork = unitOfWork;
        }
    }
}
