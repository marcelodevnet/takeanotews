﻿using TakeaNoteWS.Domain.Contracts.Repositories;
using TakeaNoteWS.Domain.Contracts.Services;
using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Domain.Services
{
    public class NoteDomainService : BaseDomainService<Note, int>, INoteDomainService
    {
        private readonly IUnitOfWorkEF unitOfWork;

        public NoteDomainService(IUnitOfWorkEF unitOfWork) : base(unitOfWork.NoteRepository)
        {
            this.unitOfWork = unitOfWork;
        }
    }
}
