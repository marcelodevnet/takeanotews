﻿using System.Collections.Generic;
using System.Security.Claims;

namespace TakeaNoteWS.Domain.Contracts
{
    public interface IUser
    {
        string Name { get; }
        string AuthenticationType { get; }
        bool IsAuthenticated();
        IEnumerable<Claim> GetClaimsIdentity();
        IEnumerable<string> GetUserRoles();
    }
}
