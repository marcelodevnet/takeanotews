﻿using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Domain.Contracts.Repositories
{
    public interface ICategoryRepository : IBaseRepository<Category, int>
    {
    }
}
