﻿using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Domain.Contracts.Repositories
{
    public interface INoteRepository : IBaseRepository<Note, int>
    {
    }
}
