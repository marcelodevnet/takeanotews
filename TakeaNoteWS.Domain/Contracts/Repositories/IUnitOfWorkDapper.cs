﻿using System;

namespace TakeaNoteWS.Domain.Contracts.Repositories
{
    public interface IUnitOfWorkDapper : IDisposable
    {
        void BeginTransaction();
        void Commit();
        void Rollback();

        IUserRepositoryDapper UserRepositoryDapper { get; }
        INoteRepositoryDapper NoteRepositoryDapper { get; }
        ICategoryRepositoryDapper CategoryRepositoryDapper { get; }
    }
}
