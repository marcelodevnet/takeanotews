﻿using System;

namespace TakeaNoteWS.Domain.Contracts.Repositories
{
    public interface IUnitOfWorkEF : IDisposable
    {
        void BeginTransaction();
        void Commit();
        void Rollback();

        IUserRepository UserRepository { get; }
        INoteRepository NoteRepository { get; }
        ICategoryRepository CategoryRepository { get; }
    }
}
