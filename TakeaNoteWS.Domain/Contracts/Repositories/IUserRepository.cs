﻿using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Domain.Contracts.Repositories
{
    public interface IUserRepository : IBaseRepository<User, int>
    {
    }
}
