﻿using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Domain.Contracts.Services
{
    public interface ICategoryDomainService : IBaseDomainService<Category, int>
    {
    }
}
