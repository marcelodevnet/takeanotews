﻿using System;
using System.Collections.Generic;

namespace TakeaNoteWS.Domain.Contracts.Services
{
    public interface IBaseDomainService<TEntity, TKey> : IDisposable where TEntity : class
    {
        void Insert(TEntity obj);
        //void InsertRange(ICollection<TEntity> collection);
        void Update(TEntity obj);
        void Delete(TEntity obj);
        //void Delete(Func<TEntity, bool> predicate);
        //void DeleteRange(ICollection<TEntity> collection);

        ICollection<TEntity> FindAll();
        //ICollection<TEntity> FindAll(Func<TEntity, bool> predicate);

        //TEntity Find(Func<TEntity, bool> predicate);
        TEntity FindById(TKey id);

        //int Count();
        int Count(Func<TEntity, bool> predicate);
    }
}
