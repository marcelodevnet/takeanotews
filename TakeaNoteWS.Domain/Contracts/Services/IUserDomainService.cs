﻿using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Domain.Contracts.Services
{
    public interface IUserDomainService : IBaseDomainService<User, int>
    {
        User FindByLoginAndPassword(string login, string password);
    }
}
