﻿using TakeaNoteWS.Domain.Entities;

namespace TakeaNoteWS.Domain.Contracts.Services
{
    public interface INoteDomainService : IBaseDomainService<Note, int>
    {
    }
}
